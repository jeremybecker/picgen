@extends('layout')

@section('content')
    <div class="container my-5">
        <div class="row py-5 fade-out">
            <div class="col-12">
                <p class="font-weight-bold">Contact</p>
                <p>Umut Savas<br>Wilenstrasse 170<br>8832 Wilen<br>Switzerland</p>
                <p>E-Mail:</p>
                <a class="mb-5 d-block" href="javascript:linkTo_UnCryptMailto('nbjmup;jogpAdmbti.gjoefs/dpn');">info
                    [at] picgen [dot] com</a>
                <p class="font-weight-bold">Limitation of liability for internal content</p>
                <p class="mb-5">The content of our website has been compiled with meticulous care and to the best of our
                    knowledge.
                    However, We cannot assume any liability for the up-to-dateness, completeness or accuracy of any of
                    the pages.</p>
                <p class="font-weight-bold">Limitation of liability for external links</p>
                <p class="mb-5">Our website may contain links to the websites of third parties (“external links”). As
                    the
                    content of these
                    websites is not under our control, We cannot assume any liability for such external content. In all
                    cases,
                    the provider of information of the linked websites is liable for the content and accuracy of the
                    information provided. At the point in time when the links were placed, no infringements of the law
                    were
                    recognisable to us. As soon as an infringement of the law becomes known to us, We will immediately
                    remove the link in question.</p>
                <p class="font-weight-bold">
                    Copyright
                </p>
                <p class="mb-5">
                    The content and works published on this website are governed by the copyright laws of Switzerland.
                    Any
                    duplication, processing, distribution or any form of utilisation beyond the scope of copyright law
                    shall
                    require the prior written consent of the author or authors in question.
                    The pictures, videos and other media published on this website belong to Riot Games.
                </p>
                <p class="font-weight-bold">
                    Data protection
                </p>
                <p class="mb-5">
                    A visit to our website can result in the storage on our server of information about the access
                    (date,
                    time,
                    page accessed). Also the account data after a registration is stored on our server.
                    This does not represent any analysis of personal data (e.g., name, address or e-mail
                    address). If personal data are collected, this only occurs – to the extent possible – with the prior
                    consent of the user of the website. Any forwarding of the data to third parties without the express
                    consent of the user shall not take place.<br><br>
                    We would like to expressly point out that the transmission of data via the Internet (e.g., by
                    e-mail)
                    can
                    offer security vulnerabilities. It is therefore impossible to safeguard the data completely against
                    access
                    by third parties. We cannot assume any liability for damages arising as a result of such security
                    vulnerabilities.<br><br>
                    The use by third parties of all published contact details for the purpose of advertising is
                    expressly
                    excluded. We reserve the right to take legal steps in the case of the unsolicited sending of
                    advertising
                    information; e.g., by means of spam mail.
                </p>
                <p class="font-weight-bold">
                    Fair use
                </p>
                <p class="mb-5">
                    Fair use is a doctrine in the United States copyright law that allows limited use of copyrighted
                    material without requiring permission from the rights holders, such as for commentary, criticism,
                    news reporting, research, teaching or scholarship. It provides for the legal, non-licensed citation
                    or incorporation of copyrighted material in another author’s work under a four-factor balancing
                    test. The term “fair use” originated in the United States.
                </p>
                <p class="font-weight-bold">
                    Creative commons
                </p>
                <p class="mb-5">
                    Picgen is provided under the terms of this creative commons public license.
                    The work is protected by copyright and/or other applicable law. Any use of
                    the work other than as authorized under this license or copyright law is prohibited.
                    By exercising any rights to the work provided here, you accept and agree to be bound by the terms of
                    this license. To the extent this license may be considered to be a contract, the licensor grants you
                    the rights contained here in consideration of your acceptance of such terms and conditions.
                </p>
                <p class="font-weight-bold">
                    Public domain
                </p>
                <p class="mb-5">
                    The people who associated a work with this deed have dedicated the work to the public domain by
                    waiving all of their rights to the work worldwide under copyright law, including all related
                    and neighboring rights, to the extent allowed by law.
                    You can copy, modify, distribute and perform the work, even for commercial purposes, all without
                    asking permission.
                </p>
            </div>
        </div>
    </div>
@endsection
