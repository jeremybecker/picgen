<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); })->name('welcome');
Route::get('/about', function() { return view('about'); })->name('about');
Route::get('/how-to-use', function() { return view('tutorial'); })->name('tutorial');
Route::get('/documentation', function() { return view('documentation'); })->name('documentation');
Route::get('/legal-notice', function() { return view('impressum'); })->name('legal-notice');
