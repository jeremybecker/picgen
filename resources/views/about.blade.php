@extends('layout')

@section('content')
    <div class="container">
        <div class="row my-5">
            <div class="col-12">
                <h2 class="display-4 text-center">About us</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-10 col-md-8 mx-auto" style="margin-bottom: 75px">
                <p>We are students at the TBZ (Technische Berufsschule Zürich). We are currently learning how to use
                    image, video and audio editing. For this purpose we wanted to create a website which other web
                    developers could use it to speed up development and save their valuable time. We also wanted to learn how to implement a
                    REST API with Laravel and thought it would be a great idea to create this service.<br><br>
                    Please keep in mind that this website is a school project. We don't take any responsibility for the
                    availability and further development.
                    The main goal for this website is to improve our coding skills learn how to edit media with
                    professional editing programs.
                    We hope you'll have fun with Picgen and can use it for your projects in the future.
                </p>
            </div>
        </div>
    </div>
@endsection
