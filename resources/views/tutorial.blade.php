@extends('layout')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="display-4 text-center my-5">Get started</h2>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-12">
                <p>The default width and height for the images are: 4032 x 3024 for landscape and 3024 x 4032 for portrait mode</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h4>Get random picture without effects</h4>
                <code>
                    http://picgen.ddns.net/api/v1/random/picture/{type}/{width}/{height}
                </code>
                <p class="my-3">Example</p>
                <code>
                    http://picgen.ddns.net/api/v1/random/picture/jpg/600/400
                </code>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <h4>Get blurred picture</h4>
                <code>
                    http://picgen.ddns.net/api/v1/blurred/picture/{type}/{width}/{height}/{blurFactor}
                </code>
                <p class="my-3">Example</p>
                <code>
                    http://picgen.ddns.net/api/v1/blurred/picture/png/600/400/100
                </code>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <h4>Get picture with negated colors</h4>
                <code>
                    http://picgen.ddns.net/api/v1/negate/picture/{type}/{width}/{height}/
                </code>
                <p class="my-3">Example</p>
                <code>
                    http://picgen.ddns.net/api/v1/negate/picture/jpg/600/400/
                </code>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <h4>Get picture with grayscale</h4>
                <code>
                    http://picgen.ddns.net/api/v1/blackwhite/picture/{type}/{width}/{height}/
                </code>
                <p class="my-3">Example</p>
                <code>
                    http://picgen.ddns.net/api/v1/blackwhite/picture/bmp/600/400/
                </code>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <h4>Get mp4 video</h4>
                <code>
                    http://picgen.ddns.net/api/v1/video/mp4
                </code>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <h4>Get avi video</h4>
                <code>
                    http://picgen.ddns.net/api/v1/video/avi
                </code>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <h4>Get wmv video</h4>
                <code>
                    http://picgen.ddns.net/api/v1/video/wmv
                </code>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <h4>Get mov video</h4>
                <code>
                    http://picgen.ddns.net/api/v1/video/mov
                </code>
            </div>
        </div>
    </div>
@endsection
