@extends('layout')

@section('content')
    <div class="container mt-5" style="margin-bottom: 100px">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center display-4" data-0="opacity:1;top:3%;transform:rotate(0deg);" data-100="opacity:0;top:-10%;transform:rotate(-90deg);">Save your time with PicGen!</h1>
            </div>
        </div>
            <div class="arrow bounce mx-auto my-5"></div>
        <div class="row" style="margin-bottom: 20vh">
            <div class="col-md-6 col-sm-12 mb-5">
                <h2>What is PicGen?</h2>
                <p class="mb-4">
                    A simple service that lets you quickly use pictures as placeholders in your website layouts.
                    We provide 3 different picture formats: JPG, PNG and BMP.
                    You can also try out our different picture effects! This service is free and simple to use.
                    If you want to get started.
                </p>
                <a href="{{route('tutorial')}}" class="btn-custom mb-5">click here</a>
            </div>
            <div class="col-md-6 col-sm-12">
                <img src="https://picsum.photos/1200/800" class="img-fluid">
            </div>
        </div>
        <div class="row" data-1000-end="transform: scale(0)" data-0-end="transform: scale(1)">
            <div class="col-md-6 col-sm-12 mb-3 order-md-last">
                <h2>Picture effects</h2>
                <p>We provide these picture effects.</p>
                <ul>
                    <li>Blurred</li>
                    <li>Negated colors</li>
                    <li>Grayscale</li>
                </ul>
            </div>
	    <div class="col-md-6 col-sm-12">
                <img src="https://picsum.photos/1200/801" class="img-fluid">
            </div>
        </div>
    </div>

@endsection
