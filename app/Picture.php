<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    public function effect() {
        return $this->hasOne(Effect::class, 'effect');
    }
}
