<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/v1/random/picture/{type}/{width}/{height}', 'PictureController@random');
Route::get('/v1/blurred/picture/{type}/{width}/{height}/{blurFactor}', 'PictureController@blurred');
Route::get('/v1/negate/picture/{type}/{width}/{height}/', 'PictureController@negate');
Route::get('/v1/blackwhite/picture/{type}/{width}/{height}/', 'PictureController@blackwhite');
Route::get('/v1/video/mp4', 'PictureController@videomp4');
Route::get('/v1/video/avi', 'PictureController@videoavi');
Route::get('/v1/video/wmv', 'PictureController@videowmv');
Route::get('/v1/video/mov', 'PictureController@videomov');
