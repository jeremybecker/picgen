<?php

namespace App\Http\Controllers;

use App\Picture;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;

const PREFIX = 'img/';

class PictureController extends Controller
{

    public function random($fileType, $width, $height)
    {
        $path = Picture::all()->random()->path;
        $randomPicture = imagecreatefromjpeg($path);
        list($x, $y, $type, $attr) = getimagesize($path);
        $randomPicture = imagecrop($randomPicture, ['x' => 0, 'y' => 0, 'width' => $width, 'height' => $height]);
        $filename = uniqid(rand(), true) . '.' . $fileType;
        switch ($fileType) {
            case "jpg":
                imagejpeg($randomPicture, PREFIX . $filename);
                break;

            case "png":
                imagepng($randomPicture, PREFIX . $filename);
                break;

            case "bmp":
                imagebmp($randomPicture, PREFIX . $filename);
                break;

            default:
                imagejpeg($randomPicture, PREFIX . $filename);
                break;
        }
        imagedestroy($randomPicture);
        return Response::download(PREFIX . $filename);
    }

    public function blurred($fileType, $width, $height, $blurFactor)
    {
        $path = Picture::all()->random()->path;
        $picture = imagecreatefromjpeg($path);
        list($x, $y, $type, $attr) = getimagesize($path);
        $picture = imagecrop($picture, ['x' => 0, 'y' => 0, 'width' => $width, 'height' => $height]);
        for ($x = 1; $x <= $blurFactor; $x++) {
            imagefilter($picture, IMG_FILTER_GAUSSIAN_BLUR);
        }
        $filename = uniqid(rand(), true) . '.' . $fileType;
        switch ($fileType) {
            case "jpg":
                imagejpeg($picture, PREFIX . $filename);
                break;
            case "png":
                imagepng($picture, PREFIX . $filename);
                break;
            case "bmp":
                imagebmp($picture, PREFIX . $filename);
                break;
            default:
                imagejpeg($picture, PREFIX . $filename);
                break;
        }
        imagedestroy($picture);
        return Response::download(PREFIX . $filename);
    }

    public function negate($fileType, $width, $height)
    {
        $path = Picture::all()->random()->path;
        $picture = imagecreatefromjpeg($path);
        list($x, $y, $type, $attr) = getimagesize($path);
        $picture = imagecrop($picture, ['x' => 0, 'y' => 0, 'width' => $width, 'height' => $height]);
        imagefilter($picture, IMG_FILTER_NEGATE);
        $filename = uniqid(rand(), true) . '.' . $fileType;
        switch ($fileType) {
            case "jpg":
                imagejpeg($picture, PREFIX . $filename);
                break;
            case "png":
                imagepng($picture, PREFIX . $filename);
                break;
            case "bmp":
                imagebmp($picture, PREFIX . $filename);
                break;
            default:
                imagejpeg($picture, PREFIX . $filename);
                break;
        }
        imagedestroy($picture);
        return Response::download(PREFIX . $filename);
    }

    public function blackWhite($fileType, $width, $height)
    {
        $path = Picture::all()->random()->path;
        $picture = imagecreatefromjpeg($path);
        list($x, $y, $type, $attr) = getimagesize($path);
        $picture = imagecrop($picture, ['x' => 0, 'y' => 0, 'width' => $width, 'height' => $height]);
        imagefilter($picture, IMG_FILTER_GRAYSCALE);
        $filename = uniqid(rand(), true) . '.' . $fileType;
        switch ($fileType) {
            case "jpg":
                imagejpeg($picture, PREFIX . $filename);
                break;
            case "png":
                imagepng($picture, PREFIX . $filename);
                break;
            case "bmp":
                imagebmp($picture, PREFIX . $filename);
                break;
            default:
                imagejpeg($picture, PREFIX . $filename);
                break;
        }
        imagedestroy($picture);
        return Response::download(PREFIX . $filename);
    }

    public function videomp4()
    {
        return Response::download('img/video.mp4');
    }
    public function videoavi()
    {
        return Response::download('img/video.avi');
    }
    public function videowmv()
    {
        return Response::download('img/video.wmv');
    }
    public function videomov()
    {
        return Response::download('img/video.mov');
    }
}
