<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PicGen</title>

    <link rel="stylesheet" href="{{ URL::asset('/css/app.css') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/skrollr/0.6.30/skrollr.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.onload = function () {
            var s = skrollr.init();
        }
    </script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
<header>

    <nav class="navbar navbar-expand-lg navbar-dark">
        <a class="navbar-brand" href="{{ route('welcome') }}">Picgen</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="{{ route('tutorial') }}">Get started</a>
                <a class="nav-item nav-link" href="https://gitlab.com/jeremybecker/picgen">Documentation</a>
                <a class="nav-item nav-link" href="{{ route('about') }}">About us</a>
            </div>
        </div>
    </nav>
</header>

@yield('content')

<footer class="py-2 mt-5 footer">
    <div class="container">
        <div class="row">
            <div class="col-12 text-white d-flex justify-content-center">
                <span class="ml-2 d-flex align-items-center">PicGen &copy; 2020 <br class="d-md-none">All rights reserved</span>
                <a class="nav-link ml-5 text-white" href="{{ route('legal-notice') }}">{{ __('Legal notice') }}</a>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
